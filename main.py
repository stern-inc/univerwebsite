from flask import Blueprint, render_template, request, redirect, url_for
import requests

main = Blueprint('main', __name__)


@main.route('/send')
def send():
    return redirect(url_for('main.index'))


@main.route('/send', methods=['POST'])
def send_post():
    name = request.form.get('name')
    phone = request.form.get('phone')
    trable = request.form.get('trable')
    if (len(name) > 1 and len(phone) > 1 and len(trable) > 1):
        text = f'✉ Поступило новое обращение ✉\
            \nИмя - {name}\
            \nНомер - {phone}\
            \nПроблема: {trable}'
        requests.get(
            f'https://api.telegram.org/bot6054996953:AAGpE_rVV3X2uyJog4InBWL-yu3ylbPD5CY/sendMessage?chat_id=-882142348&text={text}')
    return redirect(url_for('main.index'))


@main.route('/')
def index():
    return render_template('index.html')
